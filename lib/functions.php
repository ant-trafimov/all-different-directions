<?php
/**
 * AllDiff functions and definitions
 *
 * @package Test
 * @subpackage AllDiff
 * @since 1.0
 */

/**
 * Parse raw cases data
 * @param string $casesRaw
 *
 * @return array
 * @throws Exception
 */
function parseCases($casesRaw = '') {
	if(validateCases($casesRaw)) {
		$routesRaw = preg_split("/(\r\n|\n\r|\n|\r)/", $casesRaw);
		$parseError = false;
		$cases = [];

		$cntRoutes = 0;
		$routes = [];
		foreach ( $routesRaw as $data ) {
			$data = trim($data);

			if(is_numeric($data)){
				if((int)$cntRoutes !== sizeof($routes)){
					$parseError = true;
					break;
				}

				if(!empty($routes)){
					$cases[] = $routes;
				}

				$cntRoutes = $data;
				$routes = [];
			} else {
				$routes[] = parseRoute($data);
			}
		}

		if(!$parseError){
			return $cases;
		}
	}

	throw new Exception('Invalid raw data');
}

/**
 * Parse raw route data
 * @param string $routeRaw
 *
 * @return array instructions
 */
function parseRoute($routeRaw = '') {
	$routeRaw = preg_replace("/\s{2,}/", " ", $routeRaw);
	$instructions = array_chunk(explode(' ', $routeRaw), 2);
	$route['startPoints'] = array_shift($instructions);

	foreach ($instructions as $instruction){
		if(in_array($instruction[0], ['start', 'turn']) !== false){
			$route['instructions'][] = [(double)$instruction[1], 0];
		} else {
			$route['instructions'][] = [0, (double)$instruction[1]];
		}
	}

	return $route;
}

/**
 * Validate raw cases data
 * @param string $caseRaw
 *
 * @return bool
 */
function validateCases($caseRaw) {
	$numRange = "( -? ([0-9]|[1-9][0-9]|[1-9][0-9][0-9]|1000) ([\.][0-9]{1,4})? )";
	$regExp = "/^ ( ([\r\n][\n])? ([1-9]|1[0-9]|20) \s* ( [\r\n][\n] $numRange \s+ $numRange \s+ start \s $numRange ( \s+ ( walk | turn ) \s+ $numRange \s* ){1,24} )+ )+ ([\r\n][\n])0 \z $/uUx";

	return (bool)filter_var($caseRaw, FILTER_VALIDATE_REGEXP, [ 'options' => ['regexp' => $regExp] ]);
}

/**
 * Calculate next point of the route
 *
 * @param float $x x-coordinate of current position
 * @param float $y y-coordinate of current position
 * @param float $angle angle of rotation in degrees
 * @param float $length length of a vector
 *
 * @return array step coordinates [x, y]
 */
function makeStep($x, $y, $angle, $length) {
	$dx = $length*cos(deg2rad($angle));
	$dy = $length*sin(deg2rad($angle));

	return [ $x + $dx, $y + $dy];
}

/**
 * Calculate route endpoint
 *
 * @param float $x x-coordinate of current position
 * @param float $y y-coordinate of current position
 * @param array $instructions
 * [
 *     [angle1, length1],
 *     [angle2, length2],
 *     ...
 * ]
 *
 * @return array endpoint coordinates [x, y]
 */
function getEndpoint($x, $y, array $instructions) {
	$angle = 0;
	foreach ($instructions as $key => $step){
		$angle += $step[0];
		$angle = $angle > 360 ? $angle - 360 : $angle;
		list($x, $y) = makeStep($x, $y, $angle, $step[1]);
	}

	return [$x, $y];
}

/**
 * Calculate routes endpoints
 *
 * @param array $routes
 * [
 *     'startPoints' => [x, y],
 *     'instructions' => [
 *         [angle1, length1],
 *         [angle2, length2],
 *         ...
 *      ],
 *     ...
 * ]
 *
 * @return array endpoints
 * [
 *     [x1, y1],
 *     [x2, y2],
 *     ...
 * ]
 */
function getEndpoints(array $routes) {
	$endpoints = [];
	foreach ( $routes as $route ) {
		$endpoints[] = getEndpoint(
			$route['startPoints'][0],
			$route['startPoints'][1],
			$route['instructions']
		);
	}

	return $endpoints;
}

/**
 * Calculate average route endpoint
 *
 * @param array $endpoints routes endpoints
 * [
 *     [x1, y1],
 *     [x2, y2],
 *     ...
 * ]
 *
 * @return array [x, y]
 */
function getAvgEndpoint(array $endpoints) {
	$avgX = 0;
	$avgY = 0;
	foreach ($endpoints as $endpoint){
		$avgX += $endpoint[0];
		$avgY += $endpoint[1];
	}

	$cntEndpoints = count($endpoints);
	$avgX = round($avgX/$cntEndpoints, 4);
	$avgY = round($avgY/$cntEndpoints, 4);

	return [ $avgX, $avgY ];
}

/**
 * Calculate difference between the most worst endpoint
 * and average endpoint
 *
 * @param float $avgX average position x-coordinate
 * @param float $avgY average position y-coordinate
 * @param array $endpoints routes endpoints
 * - [x, y]
 *
 * @return float difference
 */
function getWorstEndpointDiff($avgX, $avgY, $endpoints) {
	$results = [];
	foreach ($endpoints as $endpoint){
		$results[] = sqrt(pow(($avgX - $endpoint[0]), 2) + pow(($avgY - $endpoint[1]), 2));
	}

	return round(max($results), 4);
}