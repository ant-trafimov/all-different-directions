<?php
/**
 * Examples
 *
 * @package Test
 * @subpackage AllDiff
 * @since 1.0
 */

/* @var array $examples */
$examples = [];

// generate random examples
for ($i=0; $i <= 96; $i++) {
	$routes = rand(1, 20);

	for ($k=0; $k <= $routes; $k++){
		$startPoints = [rand(-1000, 1000), rand(-1000, 1000)];
		$instructions = [];

		for($j=0; $j <= 25; $j++){
			$instructions[] = [rand(-180, 180), rand(-1000, 1000)];
		}

		$examples[$i][$k] = [
			'startPoints' => $startPoints,
			'instructions' => $instructions
		];
	}

}

// add some static examples
$examples = array_merge([
	// example 1
	[
		// direction 1
		[
			'startPoints' => [30, 40],
			'instructions' => [
				// start 90' and walk 5
				[90, 5]
			]
		],
		// direction 2
		[
			'startPoints' => [40, 50],
			'instructions' => [
				// 1. start 180' and walk 10
				[180, 10],
				// 2. turn 90' and walk 5
				[90, 5]
			]
		],
	],

	// example 2
	[
		// direction 1
		[
			'startPoints' => [87.342, 34.30],
			'instructions' => [
				// 1. start 0 and walk 10
				[0, 10]
			]
		],
		// direction 2
		[
			'startPoints' => [2.6762, 75.2811],
			'instructions' => [
				// 1. start -45 and walk 40
				[-45, 40],
				// 2. turn 40 and walk 60
				[40, 60]
			]
		],
		// direction 3
		[
			'startPoints' => [58.518, 93.508],
			'instructions' => [
				// 1. start 270 and walk 50
				[270, 50],
				// 2. start 90 and walk 40
				[90, 40],
				// 3. start 13 and walk 5
				[13, 5]
			]
		]
	],

	// example 3
	[
		// direction 1
		[
			'startPoints' => [80, 40],
			'instructions' => [
				// 1. start 180 walk 10
				[180, 10],
				// 2. walk 10
				[0, 10],
				// turn -90 walk 20
				[-90, 20]
			]
		],
		// direction 2
		[
			'startPoints' => [40, 75],
			'instructions' => [
				// 1. start 0 walk 20
				[0, 20],
				// 2. turn -90 and walk 15
				[-90, 15]
			]
		],
		// direction 3
		[
			'startPoints' => [90, 80],
			'instructions' => [
				// 1. start 180 walk 10
				[180, 10],
				// 2. walk 20
				[0, 20],
				// 3. turn 90 and walk 20
				[90, 20]
			]
		]
	]
], $examples);

return $examples;