<?php
/**
 * @package Test
 * @subpackage AllDiff
 * @since 1.0
 */

require ('lib/functions.php');

// parse raw data
$parseError = false;
$casesRaw = '';
$cases = [];

if(isset($_POST['casesRaw'])){
	$casesRaw = trim($_POST['casesRaw']);

	try{
		$cases = parseCases($casesRaw);
	} catch (Exception $e){
		$parseError = true;
	}
}

// prepare log
/* @var string $output*/
$output = '';

foreach ($cases as $case){
	$endpoints = getEndpoints($case);
	$avgEndpoint = getAvgEndpoint($endpoints);
	$worstEndpointDiff = getWorstEndpointDiff($avgEndpoint[0], $avgEndpoint[1], $endpoints);
	$output .= $avgEndpoint[0].' '.$avgEndpoint[1].' '.$worstEndpointDiff.'<br/>';
}
?>

<style>
	textarea{
		width: 500px;
		height: 150px;
		max-width: 100%;

	}
	.msg-error{
		color: red;
	}
</style>

<h3>Input:</h3>

<form action="" method="post" role="form">
	<textarea id="field-instructions" name="casesRaw"><?=$casesRaw?></textarea><br/><br/>
	<button type="submit">Parse</button>
</form>
<?=($parseError ? '<p class="msg-error">Parse error: Invalid route</p>' : '')?>

<h3>Output:</h3>
<?=$output;?>